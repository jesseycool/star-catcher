﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelBuilder : MonoBehaviour
{

    private int[] m_parts = { 2, 3, 4, 5, 6 };
    //private int[] m_hardParts = { 5, 6 };

    private int m_amountOfPartsOnScreen = 5;
    private float m_partHeight = 10f;
    private float m_spawnY;

    private IEnumerator Start()
    {
        for (int i = 0; i < m_amountOfPartsOnScreen; i++)
        {
            yield return StartCoroutine(LoadPart(GetRandomPart(m_parts)));
        }
    }

    public void SpawnPart()
    {
        StartCoroutine(LoadPart(GetRandomPart(m_parts)));
    }

    private int GetRandomPart(int[] list)
    {
        System.Random rand = new System.Random(Guid.NewGuid().GetHashCode()); // True random
        int i = rand.Next(0, list.Length);
        return list[i];
    }

    private IEnumerator LoadPart(int scene)
    {
        yield return SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
        GameObject[] pp = GameObject.FindGameObjectsWithTag(Gamemanager.PART_PIVOT_TAG);
        GameObject pivot = pp[pp.Length - 1];
        SceneManager.MergeScenes(SceneManager.GetSceneAt(SceneManager.sceneCount - 1), SceneManager.GetActiveScene());

        pivot.transform.position = Vector2.up * m_spawnY;
        m_spawnY += m_partHeight;
    }
}
