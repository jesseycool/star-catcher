﻿using UnityEngine;

public class Guidelines : MonoBehaviour {

    [SerializeField] private Vector2 width;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;

        Vector3 left = new Vector3(width.x, transform.position.y);
        Gizmos.DrawLine(left, new Vector3(left.x, left.y + 100));

        Gizmos.color = Color.red;

        Vector3 right = new Vector3(width.y, transform.position.y);
        Gizmos.DrawLine(right, new Vector3(right.x, right.y + 100));
    }
}