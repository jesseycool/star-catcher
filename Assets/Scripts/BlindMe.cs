﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class BlindMe : MonoBehaviour {

    private void Start()
    {
        gameObject.SetActive(false);
    }
}
