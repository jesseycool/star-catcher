﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideTutoral : MonoBehaviour {

    [SerializeField] private Gamemanager gamemanager;
    [SerializeField] private float m_hideAftherHeight = 10f;

    private void Start()
    {
        gamemanager.OnHeightUpdate += OnHeightUpdate;
    }

    private void OnHeightUpdate(float height)
    {
        if(height > m_hideAftherHeight)
        {
            Destroy(gameObject);
        }
    }

    private void OnGUI()
    {
        if(gamemanager.GameOver1)
        {
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        gamemanager.OnHeightUpdate -= OnHeightUpdate;
    }
}
