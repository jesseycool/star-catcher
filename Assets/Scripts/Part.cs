﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StarCatcher
{
    public class Part : MonoBehaviour
    {

        private void Start()
        {
            gameObject.layer = Gamemanager.DESTROYABLE_LAYER;
            gameObject.tag = Gamemanager.PART_PIVOT_TAG;

            BoxCollider2D boxCollider2D = gameObject.AddComponent(typeof(BoxCollider2D)) as BoxCollider2D;
            boxCollider2D.isTrigger = true;
        }
    }
}
