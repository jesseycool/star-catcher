﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class HolidaySpriteRenderer : MonoBehaviour {

    [SerializeField] private Sprite m_deafult;
    [SerializeField] private Sprite[] m_christmas;
    private Gamemanager gamemanager;
    private SpriteRenderer m_renderer;

    private void Awake()
    {
        m_renderer = GetComponent<SpriteRenderer>();
    }

    private Sprite GetRandomSprite(Sprite[] list)
    {
        System.Random rand = new System.Random(Guid.NewGuid().GetHashCode()); // True random
        int i = rand.Next(0, list.Length);
        return list[i];
    }

    private void Start()
    {
        gamemanager = Gamemanager.instance;
        switch (gamemanager.CurrentHoliday)
        {
            case Holiday.NONE:
                m_renderer.sprite = m_deafult;
                break;
            case Holiday.CHRISTMAS:
                m_renderer.sprite = GetRandomSprite(m_christmas);
                break;
        }
    }
}
