﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlatformType
{
    NORMAL,
    BROKEN,
    MOVING
}

public class Platform : MonoBehaviour
{

    [SerializeField] private float m_jumpForce = 400f;
    [SerializeField] private PlatformType m_type = PlatformType.NORMAL;
    private bool m_touchedPlatform;

    [Header("Werkt alleen als Type op MOVING staat")]
    [SerializeField] private Transform[] m_nodes;
    [SerializeField] private float speed = 0.1f;
    private bool m_isMoving;
    private int m_nodeIndex;
    private Transform previous; //previous waypoint
    private Transform next; //next waypoint
    private float adjustment = 0; //waypointsPassed / speed
    private float weight = 0; //weight

    [Header("Werkt alleen als Type op BROKEN staat")]
    [SerializeField] private float m_respawnTime = 3f;
    [SerializeField] private GameObject m_partical;
    private Renderer m_renderer;
    private Collider2D m_collider2D;

    public PlatformType Type
    {
        get
        {
            return m_type;
        }
    }

    private void Awake()
    {
        m_renderer = GetComponent<Renderer>();
        m_collider2D = GetComponent<Collider2D>();
    }

    private void Start()
    {
        if (m_type == PlatformType.MOVING)
        {
            if (m_nodes.Length < 2)
            {
                Debug.LogWarning("CALL JESSEY FOR HELP: Platform missing waypionts");
                m_type = PlatformType.NORMAL;
            }
        }
    }

    private void Update()
    {
        if (m_type == PlatformType.MOVING)
        {
            previous = m_nodes[m_nodeIndex]; //Where we last were
            if (m_nodeIndex != m_nodes.Length - 1) next = m_nodes[m_nodeIndex + 1]; //Where we're going
            else next = m_nodes[0]; //go back to the start
                                    //that cosine stuff is kind of nonsense so I recommend just using smoothstep
                                    //but if you must, you could swap source and destination or do angle addition etc.
            weight = (Time.time - adjustment) * 0.1f;
            if (weight > 1)
            { //We've just passed a waypoint
                weight -= 1; //cut back the weight
                adjustment += 1 / speed; //adjust the time
                if (m_nodeIndex < m_nodes.Length - 1) m_nodeIndex++; //we're at  the next point
                else m_nodeIndex = 0; //we're back to the start
            }
            transform.position = Vector3.Lerp(previous.position, next.position, Mathf.SmoothStep(0, 1, weight));
        }
    }

    private void OnDrawGizmos()
    {
        if (m_type == PlatformType.MOVING)
        {
            Gizmos.color = Color.yellow;
            for (int i = 0; i < m_nodes.Length; i++)
            {
                if ((i + 1) < m_nodes.Length)
                {
                    Transform next = m_nodes[i + 1];
                    Gizmos.DrawLine(m_nodes[i].position, next.position);
                }
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Transform other = collision.collider.transform;

        Vector3 up = transform.TransformDirection(Vector3.up);
        Vector3 toPlayer = other.position - transform.position;

        Popetje player = collision.collider.GetComponent<Popetje>();
        Rigidbody2D body = collision.collider.GetComponent<Rigidbody2D>();
        if (body != null)
        {
            //Debug.Log(Vector3.Dot(up, toPlayer).ToString());
            if (Vector3.Dot(up, toPlayer) > Gamemanager.DOT_PRODUCT)
            {
                player.JumpAnimation();
                body.AddForce(Vector2.up * m_jumpForce);
                m_touchedPlatform = true;
                if (m_type == PlatformType.BROKEN)
                {
                    if (m_touchedPlatform)
                    {
                        //play broken animatie
                        StartCoroutine(DisableMeFor(m_respawnTime));
                    }
                }
            }
        }
    }

    IEnumerator DisableMeFor(float seconds)
    {
        int cloudsBroken = PlayerPrefs.GetInt(Gamemanager.BROKEN_CLOUDS_KEY);
        cloudsBroken++;
        PlayerPrefs.SetInt(Gamemanager.BROKEN_CLOUDS_KEY, cloudsBroken);

        float progress = cloudsBroken / 50 * 100;
        if (Social.localUser.authenticated && progress >= 100)
        {
            Social.ReportProgress(GPGSIds.achievement_clouds, progress, (bool success) => {
                if (!success)
                {
                    Debug.Log("JESSEY: ReportProgress achievement_clouds failed");
                }
            });
        }

        GameObject go = Instantiate(m_partical, transform.position, transform.rotation) as GameObject;
        Destroy(go, 3f);

        m_renderer.enabled = m_collider2D.enabled = false;
        yield return new WaitForSeconds(seconds);
        m_renderer.enabled = m_collider2D.enabled = true;
    }
}
