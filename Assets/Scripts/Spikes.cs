﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour {

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Transform other = collision.collider.transform;

        Vector3 up = transform.TransformDirection(Vector3.up);
        Vector3 toPlayer = other.position - transform.position;

        Popetje player = collision.collider.GetComponent<Popetje>();
        if (player != null)
        {
            if (Vector3.Dot(up, toPlayer) > Gamemanager.DOT_PRODUCT)
            {
                player.GotHit();
            }
        }
    }
}
