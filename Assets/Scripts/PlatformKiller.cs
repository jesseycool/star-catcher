﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class PlatformKiller : MonoBehaviour {

    [SerializeField] private LevelBuilder m_levelBuilder;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject go = collision.gameObject;
        if (go.layer == Gamemanager.DESTROYABLE_LAYER)
        {
            if(go.tag == Gamemanager.PART_PIVOT_TAG)
            {
                m_levelBuilder.SpawnPart();
            }
            else
            {
                Destroy(collision.gameObject);
            }
        }
    }
}
