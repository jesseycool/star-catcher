﻿using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

namespace StarCatcher
{

    public class PlayService : MonoBehaviour
    {
        private static bool created = false;

        public delegate void PlayServiceLoginDelegate(bool success);
        public event PlayServiceLoginDelegate OnPlayServiceLogin;

        void Awake()
        {
            if (!created)
            {
                DontDestroyOnLoad(gameObject);
                created = true;
            }
        }

        private void Start()
        {
            if (!Social.localUser.authenticated) Init();
        }

        public void Init()
        {
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();

            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.DebugLogEnabled = Debug.isDebugBuild;
            PlayGamesPlatform.Activate();

            if (!Debug.isDebugBuild)
            {
                SignIn();
            }
        }

        public void SignIn()
        {
            Social.localUser.Authenticate((bool success) =>
            {
                if (success)
                {
                    ((GooglePlayGames.PlayGamesPlatform)Social.Active).SetGravityForPopups(Gravity.BOTTOM);
                    Debug.Log("JESSEY: User authenticated");
                }
                else
                {
                    Debug.Log("JESSEY: User not authenticated");
                }

                if (OnPlayServiceLogin != null) //UI event
                {
                    OnPlayServiceLogin(success);
                }
            });
        }

        public void ToAchievement()
        {
            if (Social.localUser.authenticated)
            {
                Social.ShowAchievementsUI();
            }
            else
            {
                Debug.Log("JESSEY: User not authenticated");
            }
        }

        public void ToLeaderboard()
        {
            if (Social.localUser.authenticated)
            {
                PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGSIds.leaderboard_wolrd_wide);
            }
            else
            {
                Debug.Log("JESSEY: User not authenticated");
            }
        }
    }
}
