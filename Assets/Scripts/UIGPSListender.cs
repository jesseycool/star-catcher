﻿using StarCatcher;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class UIGPSListender : MonoBehaviour {

    [SerializeField] private PlayService m_playService;
    private Button m_button;

    private void Awake()
    {
        m_button = GetComponent<Button>();
        m_playService.OnPlayServiceLogin += OnPlayServiceLogin;
    }

    private void Start()
    {
        m_button.interactable = Social.localUser.authenticated;
    }

    private void OnPlayServiceLogin(bool success)
    {
        m_button.interactable = success;
    }

    private void OnDestroy()
    {
        m_playService.OnPlayServiceLogin -= OnPlayServiceLogin;
    }
}
