﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class LevelMenu : MonoBehaviour {

    public const string MUTE_KEY = "Mute";
    public const string VIBRATE_KEY = "Vibrate";
    [SerializeField] private AudioMixer m_audioMixer;
    [SerializeField] private Toggle m_muteToggle, m_vibrateToggle;

    private void Awake()
    {
        m_muteToggle.onValueChanged.AddListener(delegate {
            if(m_muteToggle.isOn)
            {
                SetVolume(1);
                PlayerPrefs.SetInt(MUTE_KEY, 1);
            }
            else
            {
                SetVolume(0);
                PlayerPrefs.SetInt(MUTE_KEY, -1);
            }
        });

        m_vibrateToggle.onValueChanged.AddListener(delegate {
            int save = m_vibrateToggle.isOn ? 1 : -1;
            PlayerPrefs.SetInt(VIBRATE_KEY, save);
#if !UNITY_EDITOR
        if(PlayerPrefs.GetInt(LevelMenu.VIBRATE_KEY) == 1)
        {
            Handheld.Vibrate();
        }
#endif
        });
    }

    private void Start()
    {
        if(PlayerPrefs.GetInt(MUTE_KEY) == 1)
        {
            m_muteToggle.isOn = true;
        }
        else if (PlayerPrefs.GetInt(MUTE_KEY) == -1)
        {
            m_muteToggle.isOn = false;
        }

        if (PlayerPrefs.GetInt(VIBRATE_KEY) == 1)
        {
            m_vibrateToggle.isOn = true;
        }
        else if (PlayerPrefs.GetInt(VIBRATE_KEY) == -1)
        {
            m_vibrateToggle.isOn = false;
        }
    }

    public void LoadLevel(int buildIndex)
    {
        SceneManager.LoadScene(buildIndex);
    }

    private void SetVolume(float musicVolume)
    {
        musicVolume = Mathf.Clamp(musicVolume, 0.00000001f, 1);
        m_audioMixer.SetFloat("Volume", Mathf.Log(musicVolume) * 20);
    }
}
