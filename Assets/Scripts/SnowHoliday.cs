﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowHoliday : MonoBehaviour {

    [SerializeField] private Gamemanager m_gamemanager;

    private void Start() {
    
        if(m_gamemanager.CurrentHoliday != Holiday.CHRISTMAS) {
            Destroy(gameObject);
            return;
        }
    }
}
