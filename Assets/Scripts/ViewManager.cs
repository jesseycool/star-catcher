﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class ViewManager : MonoBehaviour {

    [SerializeField] private Transform m_target;
    private float m_yDest; // Pos waar de cam naartoe moet
    private float m_yCurrent; // Huidige positie van camera

    [SerializeField] private GameObject m_leftBoder;
    [SerializeField] private GameObject m_rightBoder;

    [Range(1, 100)]
    [SerializeField]
    private float m_maxUp = 5;
    [Range(1, 100)]
    [SerializeField]
    private float m_maxDown = 5;

    private void Start()
    {
        if (m_leftBoder != null && m_rightBoder != null)
        {
            PositionColliders();
        }
    }

    void FixedUpdate()
    {
        m_yDest = m_target.position.y;

        //m_yDest = Mathf.Clamp(m_yDest, -m_maxUp, m_maxDown); // Zorg voor dat de camera niet buiten de ding gaat
        m_yCurrent = Mathf.Lerp(transform.position.y, m_yDest, 7 * Time.deltaTime); // Bereken de huidige positie op basis van de lerp
        transform.position = new Vector3(transform.position.x, m_yCurrent, transform.position.z); // Apply de lerp positie
    }

    void PositionColliders()
    {
        Vector3 pos = transform.position;
        Vector2 screensize = new Vector2();
        float zDepth = 0f;

        screensize.x = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0)));

        m_leftBoder.transform.position = new Vector3(pos.x - (screensize.x * 0.5f) + (m_leftBoder.transform.localScale.x * 0.5f) - m_leftBoder.transform.localScale.x,
            pos.y,
            zDepth);

        m_rightBoder.transform.position = new Vector3(pos.x + (screensize.x * 0.5f) - (m_rightBoder.transform.localScale.x * 0.5f) + m_rightBoder.transform.localScale.x,
            pos.y,
            zDepth);
    }
}
