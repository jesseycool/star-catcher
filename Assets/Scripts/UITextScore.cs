﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class UITextScore : MonoBehaviour {

    [SerializeField] private Gamemanager gamemanager;
    private TextMeshProUGUI m_text;

    private void Awake()
    {
        m_text = GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
        gamemanager.OnScoreUpdate += UpdateText;
    }

    private void UpdateText(int score)
    {
        m_text.text = score.ToString();
    }

    private void OnDestroy()
    {
        gamemanager.OnScoreUpdate -= UpdateText;
    }
}
