﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;
using System;

public enum Holiday
{
    NONE,
    CHRISTMAS
}

public class Gamemanager : MonoBehaviour
{

    public const string SCORE_KEY = "Score";
    public const string BROKEN_CLOUDS_KEY = "BrokenClouds";
    public const string FIRST_DEAD_KEY = "FirstDead";
    public const string PART_PIVOT_TAG = "PP";
    public const int DESTROYABLE_LAYER = 8;
    public const float DOT_PRODUCT = 0.8f;
    public static Gamemanager instance = null;

    [SerializeField] private int m_starsCollected;
    [SerializeField] private float m_heightInMeters;
    [SerializeField] private GameObject m_line;
    [SerializeField] private GameObject m_gameOverText;
    [SerializeField] private Holiday m_currentHoliday = Holiday.NONE;

    public bool GameOver1 { get; private set; }

    public Holiday CurrentHoliday
    {
        get
        {
            return m_currentHoliday;
        }
    }

    public delegate void ScoreUpdateDelegate(int score);
    public event ScoreUpdateDelegate OnScoreUpdate;

    public delegate void HeightUpdateDelegate(float height);
    public event HeightUpdateDelegate OnHeightUpdate;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        if (IsItChristmas())
        {
            m_currentHoliday = Holiday.CHRISTMAS;
        }

        AnalyticsEvent.debugMode = Application.isEditor; // heel belangrijk hoor
        GameOver1 = false;

        float savedScore = PlayerPrefs.GetFloat(SCORE_KEY);
        if (savedScore > 0)
        {
            GameObject go = Instantiate(m_line);
            go.transform.position = new Vector3(go.transform.position.x, savedScore, go.transform.position.z);
        }
        AnalyticsEvent.GameStart();

        if (Social.localUser.authenticated && PlayerPrefs.GetInt(FIRST_DEAD_KEY) == 1)
        {
            Social.ReportProgress(GPGSIds.achievement_i_am_back, 100.0f, (bool success) =>
            {
                if (!success)
                {
                    Debug.Log("JESSEY: ReportProgress achievement_i_am_back failed");
                }
            });
        }
    }

    private bool IsItChristmas()
    {
        DateTime myDateTime = DateTime.Now;
        int currentYear = myDateTime.Year;

        DateTime checkDate1 = new DateTime(currentYear, 11, 6); //de rest van de werld
        //if (Application.systemLanguage == SystemLanguage.Dutch)
        //{
        //    checkDate1 = new DateTime(currentYear, 12, 6);
        //}

        DateTime checkDate2 = new DateTime(currentYear, 1, 10);

        return (myDateTime >= checkDate1 || myDateTime <= checkDate2);
    }

    public void AddStar()
    {
        m_starsCollected++;
        if (OnScoreUpdate != null)
        {
            OnScoreUpdate(m_starsCollected);
        }
    }

    public void SetHeight(float height)
    {
        m_heightInMeters = height;
        if (OnHeightUpdate != null)
        {
            OnHeightUpdate(m_heightInMeters);
        }

        //google play
        if (Social.localUser.authenticated && m_heightInMeters >= 50)
        {
            Social.ReportProgress(GPGSIds.achievement_starter, 100.0f, (bool success) =>
            {
                if (!success)
                {
                    Debug.Log("JESSEY: ReportProgress achievement_starter failed");
                }
            });
        }
    }

    public void GameOver(string cause)
    {
        if (GameOver1)
        {
            return;
        }
        GameOver1 = true;
        Debug.Log("JESSEY: Game over, cause of death: " + cause);

#if !UNITY_EDITOR
        if(PlayerPrefs.GetInt(LevelMenu.VIBRATE_KEY) == 1)
        {
            Handheld.Vibrate();
        }
#endif

        #region google play service
        if (Social.localUser.authenticated)
        {
            if (cause.Equals("PlatformWithSpike"))
            {
                Social.ReportProgress(GPGSIds.achievement_spiky, 100.0f, (bool success) =>
                {
                    if (!success)
                    {
                        Debug.Log("JESSEY: ReportProgress achievement_spiky failed");
                    }
                });
            }

            if (PlayerPrefs.GetInt(FIRST_DEAD_KEY) == 0)
            {
                Social.ReportProgress(GPGSIds.achievement_too_bad, 100.0f, (bool success) =>
                {
                    if (!success)
                    {
                        Debug.Log("JESSEY: ReportProgress achievement_spiky failed");
                    }
                });

                PlayerPrefs.SetInt(FIRST_DEAD_KEY, 1);
            }
        }
        #endregion

        AnalyticsEvent.GameOver(null, new Dictionary<string, object>
        {
            { "score", m_heightInMeters },
            { "stars", m_starsCollected },
            { "cause", cause }
        });

        SaveHighScore();
    }

    private void SaveHighScore()
    {
        float savedScore = PlayerPrefs.GetFloat(SCORE_KEY);
        if (m_heightInMeters > savedScore)
        {
            PlayerPrefs.SetFloat(SCORE_KEY, m_heightInMeters);
            Debug.Log("JESSEY: yey you got a new record; old: " + savedScore.ToString() + ", new: " + m_heightInMeters.ToString());

            Analytics.CustomEvent("newRecord", new Dictionary<string, object>
            {
                { "oldScore", savedScore },
                { "newScore", m_heightInMeters }
            });

            Social.ReportScore(Mathf.RoundToInt(m_heightInMeters), GPGSIds.leaderboard_wolrd_wide, (bool success) =>
            {
                if (!success)
                {
                    Debug.Log("JESSEY: ReportScore leaderboard_wolrd_wide failed");
                }
            });
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) //Back key on andoird
        {
            SceneManager.LoadScene(0); //Menu
        }

        if (GameOver1 && Input.GetMouseButtonDown(0))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); //Game over
        }
    }

    private void OnGUI()
    {
        m_gameOverText.SetActive(GameOver1);
    }
}
