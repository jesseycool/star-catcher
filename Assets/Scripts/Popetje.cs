﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Popetje : MonoBehaviour {

    private float m_gravity = -8f; //rouned down
    private Vector3 savedVelocity;
    private float savedAngularVelocity;
    private float m_screenWidth;
    private Rigidbody2D m_body;
    private Collider2D m_collider2D;
    private SpriteRenderer m_renderer;
    private Animator m_animator;
    private bool m_isDead;
    [SerializeField] private float m_moveSpeed = 50f;
    [SerializeField] private float m_yMaxVelocity = 10f;

    [Header(" ")]
    [SerializeField] private string m_starTag = "StarPickup";
    [SerializeField] private string m_platformTag = "Platform";
    [SerializeField] private Platform m_lastPlatform = null;
    [SerializeField] private Gamemanager gamemanager;
    [SerializeField] private GameObject m_dustPartical;

    private void Awake()
    {
        m_body = GetComponent<Rigidbody2D>();
        m_collider2D = GetComponent<Collider2D>();
        m_renderer = GetComponent<SpriteRenderer>();
        m_animator = GetComponent<Animator>();
    }

    private void Start()
    {
        m_screenWidth = Screen.width;
        m_isDead = true;
        StartCoroutine(StartTimeout());

        //to fix the start idle sprite
        if (gamemanager.CurrentHoliday == Holiday.CHRISTMAS)
        {
            m_animator.SetTrigger("Christmas");
        }
    }

    private IEnumerator StartTimeout()
    {
        yield return new WaitForSeconds(1f);
        m_isDead = false;
    }

    private void Update()
    {
        VelocityCheck();

        int i = 0;
        while(i < Input.touchCount)
        {
            if(Input.GetTouch(i).position.x > m_screenWidth / 2)
            {
                Move(1.0f); //right 
            }
            if (Input.GetTouch(i).position.x < m_screenWidth / 2)
            {
                Move(-1.0f); //left
            }
            ++i;
        }
    }

    private void FixedUpdate()
    {
        //Y velocity limiter
        Vector2 velocity = m_body.velocity;
        if(velocity.y > m_yMaxVelocity)
        {
            velocity.y = m_yMaxVelocity;
            m_body.velocity = velocity;
        }

#if UNITY_EDITOR
        Move(Input.GetAxis("Horizontal"));
#endif
    }

    private void VelocityCheck()
    {
        if(m_body.velocity.y < m_gravity)
        {
            m_isDead = true;
            string cause = "Notting";
            if (m_lastPlatform != null)
            {
                cause = m_lastPlatform.gameObject.name;
                String[] parts = cause.Split(" "[0]);
                cause = parts[0];
            }

            StartCoroutine(LetMeFall());
            gamemanager.GameOver(cause);
        }
    }

    private IEnumerator LetMeFall()
    {
        yield return new WaitForSeconds(30f);

        m_body.gravityScale = 0;
        m_body.velocity = Vector2.zero;
    }

    private void Move(float directionHorizontal)
    {
        if (m_isDead) return;
        //m_body.AddForce(new Vector2(directionHorizontal * m_moveSpeed * Time.deltaTime, 0)); //To floaty
        m_body.velocity = new Vector2(directionHorizontal * m_moveSpeed * Time.deltaTime, m_body.velocity.y);
        if (directionHorizontal != 0)
        {
            if (directionHorizontal > 0)
            {
                m_renderer.flipX = true;
            }
            else
            {
                m_renderer.flipX = false;
            }
        }
    }

    public void GotHit()
    {
        m_isDead = true;
        m_collider2D.enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag.Equals(m_starTag))
        {
            gamemanager.AddStar();
            Destroy(collision.gameObject);
        }
    }

    public void JumpAnimation()
    {
        //play partcal denk ik hiero
        GameObject go = Instantiate(m_dustPartical, transform.GetChild(0).position, transform.GetChild(0).rotation) as GameObject;
        Destroy(go, 3f);

        if (gamemanager.CurrentHoliday == Holiday.CHRISTMAS)
        {
            m_animator.SetTrigger("Christmas");
        }
        else
        {
            m_animator.SetTrigger("Jump");
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals(m_platformTag))
        {
            Platform platform = collision.gameObject.GetComponent<Platform>();
            if(platform != null)
            {
                m_lastPlatform = platform;
            }
        }
    }
}
