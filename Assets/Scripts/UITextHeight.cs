﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class UITextHeight : MonoBehaviour {

    [SerializeField] private Gamemanager gamemanager;
    private TextMeshProUGUI m_text;
    private float m_personalBest;

    private void Awake()
    {
        m_text = GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
        m_personalBest = PlayerPrefs.GetFloat(Gamemanager.SCORE_KEY);
        int best = Mathf.RoundToInt(m_personalBest);
        m_text.text = "HI " + best.ToString("0000") + " 0000";

        gamemanager.OnHeightUpdate += UpdateText;
    }

    private void UpdateText(float score)
    {
        int best = Mathf.RoundToInt(m_personalBest);
        int display = Mathf.RoundToInt(score);
        m_text.text = "HI " + best.ToString("0000") + " " + display.ToString("0000");
    }

    private void OnDestroy()
    {
        gamemanager.OnHeightUpdate -= UpdateText;
    }
}
