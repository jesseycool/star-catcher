﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeightTracker : MonoBehaviour {

    [SerializeField] private Gamemanager gamemanager;
    private float m_lastY;

    private void Update()
    {
        if(transform.position.y > m_lastY) //Needs a better selution
        {
            m_lastY = transform.position.y;
            gamemanager.SetHeight(m_lastY);
        }
    }
}
