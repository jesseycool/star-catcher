﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgoundScale : MonoBehaviour {

	void Start () {
        // Make the backdrop fit perfectly with any screensize / ratio
        float screenAspect = (float)Screen.width / (float)Screen.height;
        float camHalfHeight = Camera.main.orthographicSize;
        float camHalfWidth = screenAspect * camHalfHeight;
        float camWidth = 2.0f * camHalfWidth;

        transform.localScale = new Vector2(camWidth, camWidth);
    }
}
